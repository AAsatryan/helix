package com.example.helix

import io.realm.RealmList
import io.realm.RealmObject
import io.realm.annotations.PrimaryKey

open class HelixDB : RealmObject() {
    @PrimaryKey
    private var id: Int = 0
    var success: Boolean = false
    var metadata: RealmList<MetaData>? = null
}


open class MetaData : RealmObject() {
    var category: String? = null
    var title: String? = null
    var body: String? = null
    var shareUrl: String? = null
    var coverPhotoUrl: String? = null
    var date: Int? = null
    var gallery: RealmList<HXGallery>? = null
    var video: RealmList<HXVideo>? = null
}


open class HXGallery : RealmObject() {
    var thumbnailUrl: String? = null
}

open class HXVideo : RealmObject() {
    var thumbnailUrl: String? = null
    var youtubeId: String? = null
}

