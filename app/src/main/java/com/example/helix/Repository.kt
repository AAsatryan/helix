package com.example.helix

import com.example.helix.api.ApisService
import com.example.helix.api.RetrofitClient
import io.realm.Realm
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException

object Repository {


    private var service: ApisService = RetrofitClient.getClient()
        .create(ApisService::class.java)

    suspend fun getDataAsync(): HelixDB {
        var response: HelixDB? = null
        withContext(Dispatchers.Main) {
            try {
                response = service.getDataAsync().await()
                Realm.getDefaultInstance().executeTransaction {
                    it.copyToRealmOrUpdate(response!!)
                }
            } catch (e: HttpException) {
                null
            }
        }
        return response!!
    }

}