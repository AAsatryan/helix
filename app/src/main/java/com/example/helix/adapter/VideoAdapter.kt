package com.example.helix.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import com.example.helix.HXVideo
import com.example.helix.R
import com.example.helix.utils.YOUTUBE_URL
import kotlinx.android.synthetic.main.video_item.view.*


class VideoAdapter(val items: ArrayList<HXVideo>) : RecyclerView.Adapter<VideoAdapter.ViewModel>() {


    private var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, p: Int): ViewModel {
        context = parent.context
        return ViewModel(LayoutInflater.from(parent.context).inflate(R.layout.video_item, parent, false))

    }

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val video = items[position]
        holder.videoImage.setImageURI(video.thumbnailUrl)

        holder.itemView.setOnClickListener {
            val webView = WebView(context)
            webView.loadUrl(YOUTUBE_URL+"${video.youtubeId}")
        }

    }


    inner class ViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val videoImage = itemView.youtubeVideoImage!!
    }

}