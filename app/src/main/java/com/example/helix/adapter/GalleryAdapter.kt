package com.example.helix.adapter

import android.graphics.Bitmap
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.helix.HXGallery
import com.example.helix.R
import kotlinx.android.synthetic.main.image_item.view.*

class GalleryAdapter(val items: ArrayList<HXGallery>) : RecyclerView.Adapter<GalleryAdapter.ViewModel>() {

    var mListener: OnImageClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewModel(LayoutInflater.from(parent.context).inflate(R.layout.image_item, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewModel, position: Int) {
        val imageUrl = items[position]
        holder.image.setImageURI(imageUrl.thumbnailUrl)

        holder.itemView.setOnClickListener {
            mListener?.onClick(items, position)
        }
    }


    inner class ViewModel(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.Image
    }

    interface OnImageClickListener {
        fun onClick(images: ArrayList<HXGallery>, position: Int)
    }
}