package com.example.helix.adapter

import android.support.v7.widget.RecyclerView
import android.text.format.DateFormat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.helix.MetaData
import com.example.helix.R
import kotlinx.android.synthetic.main.rw_item.view.*
import java.util.*


class NewsAdapter(val items: ArrayList<MetaData>) : RecyclerView.Adapter<NewsAdapter.ViewHolder>() {

    var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.rw_item, parent, false))

    override fun getItemCount() = items.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = items[position]

        holder.category.text = item.category
        holder.date.text = getDate(item.date?.toLong()!!)
        holder.image.setImageURI(item.coverPhotoUrl)
        holder.itemView.setOnClickListener {
            listener!!.onItemClick(position)
        }
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image = itemView.coverUrl!!
        val category = itemView.category!!
        val date = itemView.date!!

    }

    private fun getDate(time: Long): String {
        val cal = Calendar.getInstance(Locale.ENGLISH)
        cal.timeInMillis = time * 1000
        return DateFormat.format("dd-MM-yyyy", cal).toString()
    }

    interface OnItemClickListener {
        fun onItemClick(position:Int)
    }
}