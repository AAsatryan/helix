package com.example.helix

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.content.Context
import android.net.ConnectivityManager
import io.realm.Realm
import io.realm.RealmList
import io.realm.RealmResults
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class ViewModel(application: Application) : AndroidViewModel(application) {

    private val repository = Repository
    val realm = Realm.getDefaultInstance()
    val metadata = MutableLiveData<ArrayList<MetaData>>()


    fun getDataFromNetwork() {
        GlobalScope.launch(Dispatchers.Main) {
            val data = repository.getDataAsync()
            metadata.postValue(ArrayList(data.metadata))
        }
    }

    fun getDataFromLocalDB(position: Int): MetaData? {
        val data = realm.where(HelixDB::class.java).findAll()
        return data[0]?.metadata?.get(position)
    }

    fun getDataFromDB(): RealmList<MetaData>? {
        val data = realm.where(HelixDB::class.java).findAll()
        return data[0]!!.metadata
    }

    fun isConnectedToNetwork(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val activeNetwork = connectivityManager.activeNetworkInfo

        return activeNetwork != null && activeNetwork.isConnectedOrConnecting
    }

}