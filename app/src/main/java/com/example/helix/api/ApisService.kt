package com.example.helix.api

import com.example.helix.HelixDB
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Url

interface ApisService {

    @GET("/temp/json.php")
    fun getDataAsync(): Deferred<HelixDB>
}