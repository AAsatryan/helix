package com.example.helix

import android.app.Application
import com.facebook.drawee.backends.pipeline.Fresco
import io.realm.Realm
import io.realm.RealmConfiguration

class BaseApp : Application() {

    override fun onCreate() {
        super.onCreate()
        Realm.init(this)
        val config: RealmConfiguration = RealmConfiguration.Builder()
            .deleteRealmIfMigrationNeeded().build()
        Realm.setDefaultConfiguration(config)
        Fresco.initialize(this)
    }
}