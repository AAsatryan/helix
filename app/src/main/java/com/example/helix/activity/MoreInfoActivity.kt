package com.example.helix.activity

import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.Html
import android.view.WindowManager
import android.widget.Toast
import com.example.helix.*
import com.example.helix.utils.POSITION
import kotlinx.android.synthetic.main.activity_more_info.*

class MoreInfoActivity : AppCompatActivity() {


    private lateinit var viewModel: ViewModel
    private var position: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_more_info)
        window.setFlags(
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
            WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS
        )
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        position = intent?.extras?.getInt(POSITION)
        drawData(viewModel.getDataFromLocalDB(position!!))


    }

    private fun setupToolbar(title: String) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
        supportActionBar?.title = title
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun drawData(metadata: MetaData?) {
        setupToolbar(metadata?.category!!)
        imageNews.setImageURI(metadata.coverPhotoUrl)
        textTitle.text = metadata.title
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            textDescription.text = Html.fromHtml(metadata.body, Html.FROM_HTML_MODE_LEGACY)
        }

        gallery.setOnClickListener {
            if (metadata.gallery.isNullOrEmpty()) {
                Toast.makeText(this, "This Article Haven't Images", Toast.LENGTH_LONG).show()
            } else {
                val intent = Intent(this, GalleryActivity::class.java)
                intent.putExtra(POSITION, position)
                startActivity(intent)
            }

        }

        video.setOnClickListener {
            if (metadata.video.isNullOrEmpty()) {
                Toast.makeText(this, "This Article Haven't Video Material", Toast.LENGTH_LONG).show()
            } else {
                val intent = Intent(this, VideoActivity::class.java)
                intent.putExtra(POSITION, position)
                startActivity(intent)
            }

        }

    }

}
