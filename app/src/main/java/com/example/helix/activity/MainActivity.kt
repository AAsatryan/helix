package com.example.helix.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.helix.MetaData
import com.example.helix.R
import com.example.helix.ViewModel
import com.example.helix.adapter.NewsAdapter
import com.example.helix.utils.POSITION
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity(), NewsAdapter.OnItemClickListener {

    private lateinit var viewModel: ViewModel
    private var nAdapter: NewsAdapter? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        if (viewModel.isConnectedToNetwork(this)) {
            viewModel.getDataFromNetwork()
            getDataFromObserver()
        } else {
            getDataFromDB()
        }
        getDataFromObserver()
    }

    private fun drawRecyclerView(list: ArrayList<MetaData>) {
        newsRV.layoutManager = LinearLayoutManager(this)
        nAdapter = NewsAdapter(list)
        nAdapter!!.listener = this
        newsRV.adapter = nAdapter
    }

    private fun getDataFromObserver() {
        viewModel.metadata.observe(this, Observer {
            if (it != null)
                drawRecyclerView(it)
        })
    }

    private fun getDataFromDB() {
        val data = viewModel.getDataFromDB()
        drawRecyclerView(ArrayList(data))
    }

    override fun onItemClick(position: Int) {
        val intent = Intent(this, MoreInfoActivity::class.java)
        intent.putExtra(POSITION, position)
        startActivity(intent)

    }
}
