package com.example.helix.activity

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.helix.MetaData
import com.example.helix.R
import com.example.helix.ViewModel
import com.example.helix.adapter.VideoAdapter
import com.example.helix.utils.POSITION
import kotlinx.android.synthetic.main.activity_video.*

class VideoActivity : AppCompatActivity() {

    private var videoAdapter: VideoAdapter? = null
    private lateinit var viewModel: ViewModel


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video)
        val position = intent?.extras?.getInt(POSITION)
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        setupToolbar()
        drawRecyclerView(viewModel.getDataFromLocalDB(position!!))
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun drawRecyclerView(data: MetaData?) {
        videoRecyclerView.layoutManager = LinearLayoutManager(this)
        videoAdapter = VideoAdapter(ArrayList(data?.video))
        videoRecyclerView.adapter = videoAdapter
    }
}
