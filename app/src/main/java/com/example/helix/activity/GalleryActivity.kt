package com.example.helix.activity

import android.arch.lifecycle.ViewModelProviders
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import com.example.helix.HXGallery
import com.example.helix.MetaData
import com.example.helix.R
import com.example.helix.ViewModel
import com.example.helix.adapter.GalleryAdapter
import com.example.helix.utils.POSITION
import com.stfalcon.frescoimageviewer.ImageViewer
import kotlinx.android.synthetic.main.activity_gallery.*

class GalleryActivity : AppCompatActivity(), GalleryAdapter.OnImageClickListener {

    private lateinit var viewModel: ViewModel
    private var galleryAdapter: GalleryAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)
        val position = intent?.extras?.getInt(POSITION)
        viewModel = ViewModelProviders.of(this).get(ViewModel::class.java)
        drawRecyclerView(viewModel.getDataFromLocalDB(position!!))
        setupToolbar()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_arrow_back)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    private fun drawRecyclerView(data: MetaData?) {
        galleryRecyclerView.layoutManager = GridLayoutManager(this, 2)
        galleryAdapter = GalleryAdapter(ArrayList(data?.gallery))
        galleryAdapter!!.mListener = this
        galleryRecyclerView.adapter = galleryAdapter
    }

    override fun onClick(images: ArrayList<HXGallery>, position: Int) {
        val arrayOfImages = arrayListOf<String>()
        for (i in 0 until images.size) {
            arrayOfImages.add(images[i].thumbnailUrl!!)
        }
        ImageViewer.Builder(this, arrayOfImages)
            .setStartPosition(position)
            .show()
    }
}
